<?php

class Controller
{
    public int $number;

    public function __construct($number)
    {
        $this->number = $number;
    }
}

class Model
{
    public int $ten = 10;

    public function xTen(Controller $controller)
    {
        $this->ten = $controller->number * $this->ten;
    }
}

class View
{
    public function printResult(Model $model)
    {
        echo $model->ten;
    }
}

$controller = new Controller(40);
$model = new Model();
$model->xTen($controller);
$view = new View();
$view->printResult($model);
