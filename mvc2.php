<?php

class Controller
{
    public int $kg;
    public View $view;

    public function __construct($kg)
    {
        $this->kg = $kg;
        echo $kg . ' кг = ';
    }

    public function update(Model $model): View
    {
        $this->kg *= $model->gramm ;
        return $this->view = new View($this->kg);
    }
}

class Model
{
    public int $gramm = 1000;
}

class View
{
    public function __construct(int $kg)
    {
        echo $kg . ' грамм' . '<br>';
    }
}

$controller = new Controller(30);
$model = new Model();
$controller->update($model);

$controller2 = new Controller(5);
$controller2->update($model);